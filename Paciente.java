package model;

class Paciente{
	
	private String sintomas;
	private int idade;
	
	public Paciente(String sintomas){
		this.sintomas = sintomas;
	}
	
	public void setSintomas(String sintomas){
		this.sintomas = sintomas;
	}
	public String getSintomas(){
		return sintomas;
	}
	
	public void setIdade(int idade){
		this.idade = idade;
	}
	public int getIdade(){
		return idade;
	}

	public String avaliacaoServico(int nota){
		if(nota <= 3)
			return "ruin";

		if((nota > 3) && (nota <= 7))
			return "mediano";
		else
			return "bom";
	}
}
