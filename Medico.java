package model;

class Medico{
	private String especialidade;
	private String localTrabalho;
	
	public Medico(String especialidade){
		this.especialidade = especialidade;
	}

	public String getEspecialidade(){
		return especialidade;
	}
	public void setEspecialidade(String especialidade){
		this.especialidade =  especialiadade;
	}
	public String getLocalTrabalho(){
		return especialidade;
	}
	public void setLocalTrabalho(String localTrabalho){
		this.localTrabalho =  localTrabalho;
	}

	public String gerarReceita(String diagnostico){
				
		if(diagnostico == "TI") 		
			return "receitaInternacao";
			
		if(diagnostico == "TM")
			return "receitaMedicacao";

		if(diagnostico == "TR")
			return null;

		else
			return "Diagnostico nao identificado";
	}
		
	
