package model;

class Medico extends Usuario{
	private String especialidade;
	private String localTrabalho;
	
	public Medico(){
		super.nome;
	}

	public String getEspecialidade(){
		return especialidade;
	}
	public void setEspecialidade(String umEspecialidade){
		this.especialidade =  umEspecialidade;
	}
	public String getLocalTrabalho(){
		return localTrabalho;
	}
	public void setLocalTrabalho(String localTrabalho){
		this.localTrabalho =  localTrabalho;
	}

	public String gerarReceita(String diagnostico){
				
		if(diagnostico == "TI") 		
			return "receitaInternacao";
			
		if(diagnostico == "TM")
			return "receitaMedicacao";

		if(diagnostico == "TR")
			return null;

		else
			return "Diagnostico nao identificado";
	}
}
		
	
